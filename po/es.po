# Spanish translation for giara.
# Copyright (C) 2020 giara's COPYRIGHT HOLDER
# This file is distributed under the same license as the giara package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2020-2022.
# Daniel Mustieles García <daniel.mustieles@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: giara master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/giara/issues\n"
"POT-Creation-Date: 2022-02-15 07:45+0000\n"
"PO-Revision-Date: 2022-03-03 11:57+0100\n"
"Last-Translator: Daniel Mustieles García <daniel.mustieles@gmail.com>\n"
"Language-Team: Spanish - Spain <gnome-es-list@gnome.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 41.0\n"

#: data/org.gabmus.giara.appdata.xml.in:4
msgid "Giara"
msgstr "Giara"

#: data/org.gabmus.giara.appdata.xml.in:5
msgid "Gabriele Musco"
msgstr "Gabriele Musco"

#: data/org.gabmus.giara.appdata.xml.in:6 data/org.gabmus.giara.desktop.in:5
msgid "An app for Reddit"
msgstr "Una aplicación para Reddit"

#: data/org.gabmus.giara.appdata.xml.in:15
msgid "An app for Reddit."
msgstr "Una aplicación para Reddit."

#: data/org.gabmus.giara.appdata.xml.in:16
msgid "Browse Reddit from your Linux desktop or smartphone."
msgstr "Explore Reddit desde su escritorio Linux o teléfono inteligente."

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gabmus.giara.desktop.in:14
msgid "reddit;"
msgstr "reddit;"

#: data/ui/aboutdialog.ui.in:7
msgid "@authorfullname@, et al."
msgstr "@authorfullname@, et al."

#: data/ui/aboutdialog.ui.in:10
msgid "@TRANSLATORS@"
msgstr "@TRANSLATORS@"

#: data/ui/new_post_window.ui:20
msgid "New post"
msgstr "Nueva publicación"

#: data/ui/new_post_window.ui:25
msgid "Cancel"
msgstr "Cancelar"

#: data/ui/new_post_window.ui:31
msgid "Send"
msgstr "Enviar"

#: data/ui/new_post_window.ui:59
msgid "Post title…"
msgstr "Título de la publicación…"

#: data/ui/new_post_window.ui:94
msgid "Link…"
msgstr "Enlace…"

#: data/ui/new_post_window.ui:103
msgid "Select media…"
msgstr "Seleccionar medio…"

#: data/ui/post_preview.ui:71
msgid "Upvote"
msgstr "Voto positivo"

#: data/ui/post_preview.ui:85
msgid "Ups"
msgstr "Subidas"

#: data/ui/post_preview.ui:92
msgid "Downvote"
msgstr "Voto negativo"

#: data/ui/post_preview.ui:116
msgid "Pinned"
msgstr "Fijado"

#: data/ui/post_preview.ui:200
msgid "Open media"
msgstr "Abrir medio"

#: data/ui/post_preview.ui:212
msgid "Open link"
msgstr "Abrir enlace"

#: data/ui/post_preview.ui:223
msgid "Save"
msgstr "Guardar"

#: data/ui/post_preview.ui:234
msgid "Share"
msgstr "Compartir"

#: data/ui/post_preview.ui:247
msgid "Delete"
msgstr "Eliminar"

#: data/ui/subreddit_more_actions.ui:28 data/ui/subreddit_more_actions.ui:76
msgid "Add to multireddit"
msgstr "Añadir a multireddit"

#: data/ui/subreddit_more_actions.ui:64 data/ui/subreddit_more_actions.ui:124
msgid "Back"
msgstr "Atrás"

#: data/ui/subreddit_more_actions.ui:98 data/ui/subreddit_more_actions.ui:136
msgid "Create multireddit"
msgstr "Crear multireddit"

#: data/ui/subreddit_more_actions.ui:151
msgid "Multireddit name…"
msgstr "Nombre del multireddit…"

#: data/ui/subreddit_more_actions.ui:156
msgid "Create"
msgstr "Crear"

#: giara/auth.py:20
msgid "Error getting client with refresh token, retrying…"
msgstr "Error al obtener el cliente con el testigo actualizado, reintentando…"

#: giara/auth.py:30
msgid "Error authorizing Reddit client, retrying…"
msgstr "Error al autorizar el cliente de Reddit, reintentando…"

#: giara/auth.py:35
msgid "Error authorizing Reddit client after retry, quitting…"
msgstr ""
"Error al autorizar el cliente de Reddit después de reintentar, saliendo…"

#: giara/base_preferences.py:106
msgid "Choose a folder"
msgstr "Elegir una carpeta"

#: giara/choice_picker.py:52 giara/choice_picker.py:64
#: giara/new_post_window.py:241 giara/new_post_window.py:242
#: giara/new_post_window.py:248
msgid "None"
msgstr "Ninguno"

#: giara/common_post_box.py:136
msgid "Are you sure you want to delete this item?"
msgstr "¿Está seguro de querer eliminar este elemento?"

#: giara/common_post_box.py:174
msgid "Link copied to clipboard"
msgstr "Enlace copiado al portapapeles"

#: giara/common_post_box.py:312
msgid "Comment: "
msgstr "Comentario: "

#: giara/common_post_box.py:320 giara/flair_label.py:33
msgid "Message"
msgstr "Mensaje"

#: giara/common_post_box.py:327 giara/post_details_view.py:103
#: giara/inbox_view.py:36 giara/inbox_view.py:43
msgid "Author unknown"
msgstr "Autor desconocido"

#: giara/flair_label.py:25
msgid "Comment"
msgstr "Comentar"

#: giara/flair_label.py:29
msgid "Post"
msgstr "Publicar"

#: giara/flair_label.py:54
msgid "Image"
msgstr "Imagen"

#: giara/flair_label.py:60
msgid "Video"
msgstr "Vídeo"

#: giara/flair_label.py:66
msgid "Text"
msgstr "Texto"

#: giara/flair_label.py:72
msgid "Link"
msgstr "Enlace"

#: giara/flair_label.py:78
msgid "NSFW"
msgstr "NSFW"

#: giara/post_details_view.py:35
msgid "Load more comments"
msgstr "Cargar más comentarios"

#: giara/front_page_headerbar.py:22 giara/preferences_window.py:27
msgid "Best"
msgstr "Mejor"

#: giara/front_page_headerbar.py:26 giara/preferences_window.py:27
#: giara/subreddit_view.py:202 giara/single_post_stream_headerbar.py:20
msgid "Hot"
msgstr "Caliente"

#: giara/front_page_headerbar.py:30 giara/preferences_window.py:27
#: giara/inbox_view.py:71 giara/subreddit_view.py:206
#: giara/single_post_stream_headerbar.py:24
msgid "New"
msgstr "Nuevo"

#: giara/front_page_headerbar.py:34 giara/preferences_window.py:27
#: giara/subreddit_view.py:210 giara/single_post_stream_headerbar.py:28
msgid "Top"
msgstr "Superior"

#: giara/front_page_headerbar.py:38 giara/preferences_window.py:28
#: giara/subreddit_view.py:214 giara/single_post_stream_headerbar.py:32
msgid "Rising"
msgstr "Creciente"

#: giara/front_page_headerbar.py:42 giara/preferences_window.py:28
#: giara/subreddit_view.py:218 giara/single_post_stream_headerbar.py:36
msgid "Controversial"
msgstr "Polémico"

#: giara/front_page_headerbar.py:72
#, python-brace-format
msgid "{0} Karma"
msgid_plural "{0} Karma"
msgstr[0] "Karma {0}"
msgstr[1] "Karma {0}"

#: giara/preferences_window.py:16
msgid "General"
msgstr "General"

#: giara/preferences_window.py:19
msgid "General preferences"
msgstr "Preferencias generales"

#: giara/preferences_window.py:20
msgid "Default view"
msgstr "Vista predeterminada"

#: giara/preferences_window.py:33
msgid "Cache"
msgstr "Caché"

#: giara/preferences_window.py:34
msgid "Clear cache"
msgstr "Limpiar la caché"

#: giara/preferences_window.py:34
msgid "Clear"
msgstr "Limpiar"

#: giara/preferences_window.py:55
msgid "Appearance"
msgstr "Apariencia"

#: giara/preferences_window.py:58
msgid "Appearance preferences"
msgstr "Preferencias de la apariencia"

#: giara/preferences_window.py:60
msgid "Dark mode"
msgstr "Modo oscuro"

#: giara/preferences_window.py:64
msgid "Blur NSFW images"
msgstr "Imágenes NSFW difusas"

#: giara/preferences_window.py:69
msgid "Thumbnail preferences"
msgstr "Preferencias de la miniatura"

#: giara/preferences_window.py:71
msgid "Show thumbnails in post previews"
msgstr "Mostrar miniaturas en la vista previa de las publicaciones"

#: giara/preferences_window.py:76
msgid "Max thumbnail height"
msgstr "Altura máxima de las miniaturas"

#: giara/preferences_window.py:90
msgid "Privacy"
msgstr "Privacidad"

#: giara/preferences_window.py:93
msgid "Link replacement"
msgstr "Reemplazo de enlaes"

#: giara/preferences_window.py:95
msgid "Replace Twitter links with Nitter"
msgstr "Reemplazar enlaces de Twitter con Nitter"

#: giara/preferences_window.py:99
msgid "Nitter instance"
msgstr "Instancia de Nitter"

#: giara/preferences_window.py:103
msgid "Replace YouTube links with Invidious"
msgstr "Reemplazar enlaces de Youtube con Invidious"

#: giara/preferences_window.py:107
msgid "Invidious instance"
msgstr "Instancia de Invidious"

#: giara/inbox_view.py:54
#, python-brace-format
msgid "Comment in \"{0}\""
msgstr "Comentar en «{0}»"

#: giara/inbox_view.py:138 giara/left_stack.py:146
msgid "Inbox"
msgstr "Bandeja de entrada"

#: giara/left_stack.py:64
msgid "Front page"
msgstr "Página principal"

#: giara/left_stack.py:71 giara/left_stack.py:79
msgid "Saved posts"
msgstr "Publicaciones guardadas"

#: giara/left_stack.py:86 giara/left_stack.py:94
msgid "Profile"
msgstr "Perfil"

#: giara/left_stack.py:107 giara/subreddits_list_view.py:138
#: giara/search_view.py:58
msgid "Subreddits"
msgstr "Subreddits"

#: giara/left_stack.py:126
msgid "Search"
msgstr "Buscar"

#: giara/left_stack.py:156 giara/multireddit_list_view.py:106
msgid "Multireddits"
msgstr "Multireddits"

#: giara/left_stack.py:167 giara/left_stack.py:173
msgid "r/all"
msgstr "r/all"

#: giara/left_stack.py:180 giara/left_stack.py:186
msgid "r/popular"
msgstr "r/popular"

#: giara/left_stack.py:325 giara/subreddit_search_view.py:29
#, python-brace-format
msgid "Searching in {0}"
msgstr "Buscando en {0}"

#: giara/__main__.py:112
msgid "Do you want to log out? This will close the application."
msgstr "¿Quiere cerrar la sesión? Esto cerrará la aplicación."

#: giara/markdown_view.py:43
msgid "Image: "
msgstr "Imagen: "

#: giara/markdown_view.py:43
msgid "[Image]"
msgstr "[Imagen]"

#: giara/multireddit_view.py:61
msgid "Remove from multireddit"
msgstr "Quitar de multireddit"

#: giara/multireddit_view.py:87
msgid "Edit multireddit"
msgstr "Editar multireddit"

#: giara/new_post_window.py:90
msgid "Choose an image or video to upload"
msgstr "Elegir una imagen o vídeo para subir"

#: giara/new_post_window.py:113
msgid "Remove"
msgstr "Quitar"

#: giara/new_post_window.py:296
msgid "Select a subreddit…"
msgstr "Seleccionar un subreddit…"

#: giara/new_post_window.py:369
msgid "Select a flair…"
msgstr "Seleccionar «flair»…"

#: giara/new_post_window.py:434
msgid "New comment"
msgstr "Nuevo comentario"

#: giara/new_post_window.py:464
msgid "Editing"
msgstr "Editando"

#: giara/new_post_window.py:465
msgid "Edit"
msgstr "Editar"

#: giara/notification_manager.py:31
#, python-brace-format
msgid "{0} new message"
msgid_plural "{0} new messages"
msgstr[0] "{0} mensaje nuevo"
msgstr[1] "{0} mensajes nuevos"

#: giara/notification_manager.py:36
#, python-brace-format
msgid "{0} more"
msgid_plural "{0} more"
msgstr[0] "{0} más"
msgstr[1] "{0} más"

#: giara/picture_gallery.py:27
msgid "Previous picture"
msgstr "Foto anterior"

#: giara/picture_gallery.py:33
msgid "Next picture"
msgstr "Siguiente foto"

#: giara/subreddit_view.py:43
msgid "This subreddit is already part of this multireddit"
msgstr "Este subreddit ya es parte de este multireddit"

#: giara/subreddit_view.py:235
msgid "More actions"
msgstr "Más acciones"

#: giara/subreddit_view.py:276
msgid "Leave"
msgstr "Salir"

#: giara/subreddit_view.py:279
msgid "Join"
msgstr "Unirse"

#: giara/subreddit_view.py:318
msgid "Posts"
msgstr "Publicaciones"

#: giara/subreddit_view.py:324
msgid "Description"
msgstr "Descripción"

#: giara/search_view.py:64
msgid "Users"
msgstr "Usuarios"

#: giara/sort_menu_btn.py:74
#, python-brace-format
msgid "Sort by: {0}"
msgstr "Ordenar por: {0}"

#: giara/user_heading.py:52
msgid "Unfollow"
msgstr "Dejar de seguir"

#: giara/user_heading.py:55
msgid "Follow"
msgstr "Seguir"

#~ msgid "Open externally"
#~ msgstr "Abrir externamente"

#~ msgid "Welcome to Giara"
#~ msgstr "Bienvenido a Giara"

#~ msgid "org.gabmus.giara-symbolic"
#~ msgstr "org.gabmus.giara-symbolic"

#~ msgid "Login"
#~ msgstr "Iniciar sesión"

#~ msgid "Waiting for authentication…"
#~ msgstr "Esperando la autenticación…"

#~ msgid ""
#~ "The browser has not opened yet? Use the button below to copy the login "
#~ "link."
#~ msgstr ""
#~ "¿Aún no se ha abierto el navegador? Utilice el botón de abajo para copiar "
#~ "el enlace de inicio de sesión."

#~ msgid "Copy login link"
#~ msgstr "Copiar el enlace de inicio de sesión"

#~ msgid "Close"
#~ msgstr "Cerrar"

#~ msgid "Reply"
#~ msgstr "Responder"

#~ msgid "Refresh"
#~ msgstr "Actualizar"

#~ msgid "Karma"
#~ msgstr "Karma"

#~ msgid "Cake day"
#~ msgstr "Aniversario (cake day)"

#~ msgid "Open Keyboard Shortcuts"
#~ msgstr "Abrir atajos del teclado"

#~ msgid "Open Menu"
#~ msgstr "Abrir menú"

#~ msgid "Open Preferences"
#~ msgstr "Abrir preferencias"

#~ msgid "Quit"
#~ msgstr "Salir"

#~ msgid "Members"
#~ msgstr "Miembros"

#~ msgid "Since"
#~ msgstr "Desde"

#~ msgid "Collapse replies"
#~ msgstr "Contraer respuestas"

#~ msgid "Saved"
#~ msgstr "Guardado"

#~ msgid "Log Out"
#~ msgstr "Cerrar la sesión"

#~ msgid "Messages in your inbox"
#~ msgstr "Mensajes en su bandeja de entrada"

#~ msgid "Menu"
#~ msgstr "Menú"

#~ msgid "Preferences"
#~ msgstr "Preferencias"

#~ msgid "Keyboard Shortcuts"
#~ msgstr "Atajo del teclado"

#~ msgid "About Giara"
#~ msgstr "Acerca de Giara"

#~ msgid "Media"
#~ msgstr "Multimedia"

#~ msgid "General Settings"
#~ msgstr "Configuración general"

#~ msgid "View"
#~ msgstr "Ver"

#~ msgid "View Settings"
#~ msgstr "Configuración de la vista"

#~ msgid "Comments"
#~ msgstr "Comentarios"

#~ msgid "Use 0 to remove the limit"
#~ msgstr "Use 0 para quitar el límite"

#~ msgid "Naked domain name only, no https://"
#~ msgstr "Nombre del dominio a secas, sin https://"

#~ msgid "Giara is a GTK app for Reddit"
#~ msgstr "Giara es una aplicación GTK para Reddit"

#~ msgid "1 new message"
#~ msgstr "1 mensaje nuevo"
