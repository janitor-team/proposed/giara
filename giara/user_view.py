from giara.single_post_stream_view import SinglePostStreamView
from giara.user_heading import UserHeading


class UserView(SinglePostStreamView):
    def __init__(self, user, show_post_func):
        self.user = user
        self.heading = None
        super().__init__(
            self.user.new, self.user.name, 'u/'+self.user.name, show_post_func,
            sort_menu=False, load_now=True
        )
        self.refresh()

    def add_heading(self, *args):
        target_box = self
        if self.heading is not None:
            target_box.remove(self.heading)
        self.heading = UserHeading(self.user, self.refresh)
        target_box.insert_child_after(self.heading, self.headerbar)
        self.show()

    def refresh(self, *args):
        super().refresh(*args)
        self.add_heading()
