from gi.repository import Adw, Gtk


def new_clamp():
    return Adw.Clamp(
        maximum_size=1200, tightening_threshold=1000, margin_start=12,
        margin_end=12, vexpand=True
    )


def new_clamp_scrollable():
    return Adw.ClampScrollable(
        maximum_size=1200, tightening_threshold=1000, margin_start=12,
        margin_end=12, vexpand=True,
        vscroll_policy=Gtk.ScrollablePolicy.NATURAL
    )
