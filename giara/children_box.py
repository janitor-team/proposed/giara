from gi.repository import Gtk


class ChildrenBox(Gtk.Box):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__children = list()

    def get_children(self):
        return self.__children

    def append(self, child, *args, **kwargs):
        super().append(child, *args, **kwargs)
        self.__children.append(child)

    def prepend(self, child, *args, **kwargs):
        super().prepend(child, *args, **kwargs)
        self.__children.insert(0, child)

    def remove(self, child, *args, **kwargs):
        super().remove(child, *args, **kwargs)
        self.__children.remove(child)
