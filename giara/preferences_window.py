from gettext import gettext as _
from gi.repository import Gtk, Adw
from os.path import isfile, abspath, join
from os import remove, listdir
from typing import Optional
from giara.base_preferences import (
    MPreferencesPage, MPreferencesGroup,
    PreferencesButtonRow, PreferencesEntryRow, PreferencesSpinButtonRow,
    PreferencesComboRow, PreferencesToggleRow
)


class GeneralPreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('General'), icon_name='preferences-other-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('General preferences'), rows=[PreferencesComboRow(
                        title=_('Default view'),
                        conf_key='default_front_page_view',
                        values=[
                            'best', 'hot', 'new', 'top', 'rising',
                            'controversial'
                        ],
                        value_names=[
                            _('Best'), _('Hot'), _('New'), _('Top'),
                            _('Rising'), _('Controversial')
                        ]
                    )]
                ),
                MPreferencesGroup(
                    title=_('Cache'), rows=[PreferencesButtonRow(
                        title=_('Clear cache'), button_label=_('Clear'),
                        onclick=self.clear_cache,
                        button_style_class='destructive-action'
                    )]
                )
            ]
        )

    def clear_cache(self, confman):
        for p in [confman.cache_path, confman.thumbs_cache_path]:
            files = [
                abspath(join(p, f)) for f in listdir(p)
            ]
            for f in files:
                if isfile(f):
                    remove(f)


class AppearancePreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('Appearance'), icon_name='applications-graphics-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('Appearance preferences'), rows=[
                        PreferencesToggleRow(
                            title=_('Dark mode'), conf_key='dark_mode',
                            signal='dark_mode_changed'
                        ),
                        PreferencesToggleRow(
                            title=_('Blur NSFW images'), conf_key='blur_nsfw'
                        )
                    ]
                ),
                MPreferencesGroup(
                    title=_('Thumbnail preferences'), rows=[
                        PreferencesToggleRow(
                            title=_('Show thumbnails in post previews'),
                            conf_key='show_thumbnails_in_preview',
                            signal='on_show_thumbnails_in_preview_changed'
                        ),
                        PreferencesSpinButtonRow(
                            title=_('Max thumbnail height'),
                            min_v=100, max_v=2000,
                            conf_key='max_picture_height',
                            signal='on_max_picture_height_changed'
                        )
                    ]
                )
            ]
        )


class PrivacyPreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('Privacy'), icon_name='eye-not-looking-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('Link replacement'), rows=[
                        PreferencesToggleRow(
                            title=_('Replace Twitter links with Nitter'),
                            conf_key='twitter2nitter'
                        ),
                        PreferencesEntryRow(
                            title=_('Nitter instance'),
                            conf_key='nitter_instance'
                        ),
                        PreferencesToggleRow(
                            title=_('Replace YouTube links with Invidious'),
                            conf_key='youtube2invidious'
                        ),
                        PreferencesEntryRow(
                            title=_('Invidious instance'),
                            conf_key='invidious_instance'
                        )
                    ]
                )
            ]
        )


class PreferencesWindow(Adw.PreferencesWindow):
    def __init__(self, parent_win: Optional[Gtk.Window] = None):
        super().__init__()
        if parent_win:
            self.set_transient_for(parent_win)
            self.set_modal(True)
        self.pages = [
            GeneralPreferencesPage(),
            AppearancePreferencesPage(),
            PrivacyPreferencesPage()
        ]
        for p in self.pages:
            self.add(p)
        self.set_default_size(630, 700)
