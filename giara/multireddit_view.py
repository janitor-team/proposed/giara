from gettext import gettext as _
from gi.repository import Gtk, GLib
from giara.single_post_stream_view import SinglePostStreamView
from giara.subreddits_list_view import SubredditsListboxRow
from threading import Thread


class MultiredditEditPopover(Gtk.Popover):
    def __init__(self, multi, relative_to, **kwargs):
        super().__init__(**kwargs)
        self.multi = multi
        self.relative_to = relative_to
        self.set_autohide(True)
        self.set_parent(self.relative_to)
        self.set_size_request(300, 400)
        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.sw.get_style_context().add_class('card')
        self.sw.set_margin_top(6)
        self.sw.set_margin_bottom(6)
        self.sw.set_margin_start(6)
        self.sw.set_margin_end(6)
        self.sw.show()
        self.subs_lbox = Gtk.ListBox()
        self.subs_lbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.sw.set_child(self.subs_lbox)
        self.set_child(self.sw)
        self.populate()

    def empty(self):
        while True:
            row = self.subs_lbox.get_row_at_index(0)
            if row:
                self.subs_lbox.remove(row)
            else:
                break

    def populate(self):
        self.empty()

        def af():
            self.multi._fetch()
            for sub in self.multi.subreddits:
                sub._fetch()
                GLib.idle_add(cb, sub)

        def cb(sub):
            row = SubredditsListboxRow(sub)

            def on_remove_sub(*args):
                Thread(
                    target=lambda *args: self.multi.remove(sub), daemon=True
                ).start()
                self.subs_lbox.remove(row)

            row.delete_btn = Gtk.Button.new_from_icon_name(
                'user-trash-symbolic'
            )
            row.delete_btn.get_style_context().add_class('destructive-action')
            row.delete_btn.get_style_context().remove_class('image-button')
            row.delete_btn.set_tooltip_text(_('Remove from multireddit'))
            row.delete_btn.connect(
                'clicked', on_remove_sub
            )
            row.main_box.append(row.delete_btn)
            row.delete_btn.set_valign(Gtk.Align.CENTER)
            self.subs_lbox.append(row)
            self.sw.show()

        Thread(target=af, daemon=True).start()


class MultiredditView(SinglePostStreamView):
    def __init__(self, multi, show_post_func, source=None):
        self.multi = multi
        super().__init__(
            self.multi.hot,
            self.multi.display_name,
            self.multi.display_name,
            show_post_func,
            source=self.multi
        )
        self.edit_btn = Gtk.MenuButton()
        self.edit_btn.set_icon_name(
            'document-edit-symbolic'
        )
        self.edit_btn.set_tooltip_text(_('Edit multireddit'))
        self.edit_popover = MultiredditEditPopover(self.multi, self.edit_btn)
        self.edit_btn.set_popover(self.edit_popover)
        self.headerbar.headerbar.pack_end(self.edit_btn)
        self.headerbar.show()
